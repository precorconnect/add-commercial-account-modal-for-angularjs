## Description
Add account modal for AngularJS.

## Example
refer to the [example app](example) for working example code.

## Setup

**install via jspm**  
```shell
jspm install bitbucket:precorconnect/add-commercial-account-modal-for-angularjs
``` 

**import & wire up**
```js
import 'add-commercial-account-modal-for-angularjs';

angular.module(
            "app",
            ["add-commercial-account-modal.module"]
        )
        // ensure dependencies available in container
        .constant(
            'sessionManager', 
            sessionManager
            /*see https://bitbucket.org/precorconnect/session-manager-for-browsers*/
        )
        .constant(
            'accountServiceSdk',
            accountServiceSdk
            /*see https://bitbucket.org/precorconnect/account-service-sdk-for-javascript*/
        )        
        .constant(
            'iso3166Sdk',
            iso3166Sdk
            /*see https://bitbucket.org/precorconnect/iso-3166-sdk-for-javascript*/
        )
        .constant(
            'customerBrandServiceSdk',
            customerBrandServiceSdk
            /*see https://bitbucket.org/precorconnect/customer-brand-service-sdk-for-javascript*/
        )
        .constant(
            'customerSegmentServiceSdk',
            customerSegmentServiceSdk
            /*see https://bitbucket.org/precorconnect/customer-segment-service-sdk-for-javascript*/
        );
```