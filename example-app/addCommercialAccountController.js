import SessionManager from 'session-manager';
import AccountServiceSdk from 'account-service-sdk';

export default class AddCommercialAccountController {

    _sessionManager:SessionManager;

    _addCommercialAccountModal;

    _accountServiceSdk:AccountServiceSdk;

    _account;

    constructor(sessionManager:SessionManager,
                addCommercialAccountModal,
                accountServiceSdk:AccountServiceSdk) {

        if (!sessionManager) {
            throw new TypeError('sessionManager required');
        }
        this._sessionManager = sessionManager;

        if (!addCommercialAccountModal) {
            throw new TypeError('addCommercialAccountModal required');
        }
        this._addCommercialAccountModal = addCommercialAccountModal;

        if (!accountServiceSdk) {
            throw new TypeError('accountServiceSdk required');
        }
        this._accountServiceSdk = accountServiceSdk;

    }

    get account() {
        return this._account;
    }

    showAddCommercialAccountModal() {

        this._addCommercialAccountModal
            .show()
            .then(accountId => {
                    this._account = {id: accountId};
                }
            )
            .then(() => this._sessionManager.getAccessToken())
            .then(accessToken =>
                this._accountServiceSdk
                    .getCommercialAccountWithId(
                        this._account.id,
                        accessToken
                    )
            )
            .then(account => {
                    this._account = account;
                }
            );

    }

}


AddCommercialAccountController.$inject = [
    'sessionManager',
    'addCommercialAccountModal',
    'accountServiceSdk'
];
