import addCommercialAccountTemplate from './add-commercial-account.html!text';
import AddCommercialAccountController from './addCommercialAccountController';

export default class RouteConfig {

    constructor($routeProvider) {

        $routeProvider
            .when
            (
                '/',
                {
                    template: addCommercialAccountTemplate,
                    controller: AddCommercialAccountController,
                    controllerAs: 'controller'
                }
            );

    }

}

RouteConfig.$inject = [
    '$routeProvider'
];