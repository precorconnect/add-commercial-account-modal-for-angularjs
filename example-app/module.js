import angular from 'angular';
import '../src/module';
import RouteConfig from './routeConfig';
import SessionManager,{SessionManagerConfig} from 'session-manager';
import AccountServiceSdk,{AccountServiceSdkConfig} from 'account-service-sdk';
import CustomerBrandServiceSdk,{CustomerBrandServiceSdkConfig} from 'customer-brand-service-sdk'
import CustomerSegmentServiceSdk,{CustomerSegmentServiceSdkConfig} from 'customer-segment-service-sdk'
import Iso3166Sdk from 'iso-3166-sdk';
import 'angular-route';

const precorConnectApiBaseUrl = 'https://api-dev.precorconnect.com';

const sessionManager =
    new SessionManager(
        new SessionManagerConfig(
            precorConnectApiBaseUrl/* identityServiceBaseUrl */,
            'https://precor.oktapreview.com/app/template_saml_2_0/exk5cmdj3pY2eT5JU0h7/sso/saml'/* loginUrl */,
            'https://dev.precorconnect.com/customer/account/logout/'/* logoutUrl*/,
            30000/* accessTokenRefreshInterval */
        )
    );

const accountServiceSdk =
    new AccountServiceSdk(
        new AccountServiceSdkConfig(
            precorConnectApiBaseUrl
        )
    );

const customerBrandServiceSdk =
    new CustomerBrandServiceSdk(
        new CustomerBrandServiceSdkConfig(
            precorConnectApiBaseUrl
        )
    );

const customerSegmentServiceSdk =
    new CustomerSegmentServiceSdk(
        new CustomerSegmentServiceSdkConfig(
            precorConnectApiBaseUrl
        )
    );

angular
    .module(
        'exampleApp.module',
        [
            'ngRoute',
            'addCommercialAccountModal.module'
        ]
    )
    .constant(
        'sessionManager',
        sessionManager
    )
    .constant(
        'accountServiceSdk',
        accountServiceSdk
    )
    .constant(
        'iso3166Sdk',
        new Iso3166Sdk()
    )
    .constant(
        'customerBrandServiceSdk',
        customerBrandServiceSdk
    )
    .constant(
        'customerSegmentServiceSdk',
        customerSegmentServiceSdk
    )
    .config(
        [
            '$routeProvider',
            $routeProvider => new RouteConfig($routeProvider)
        ]
    );
