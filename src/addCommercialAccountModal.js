import Controller from './controller';
import './default.css!css';
import template from './index.html!text';

export default class AddCommercialAccountModal {

    _$modal;

    constructor($modal) {

        if(!$modal){
            throw new TypeError('$modal required');
        }
        this._$modal = $modal;

    }

    /**
     * Shows the add account modal
     * @returns {Promise<string>} account id
     */
    show():Promise<string> {

        let modalInstance = this._$modal.open({
            controller: Controller,
            controllerAs: 'controller',
            template: template,
            backdrop: 'static'
        });

        return modalInstance.result;
    }
}

AddCommercialAccountModal.$inject = [
    '$modal'
];