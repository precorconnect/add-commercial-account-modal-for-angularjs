import AccountServiceSdk,{AddCommercialAccountReq} from 'account-service-sdk';
import AddCommercialAccountReqFactory from './addCommercialAccountReqFactory';
import Iso3166Sdk from 'iso-3166-sdk';
import CustomerSegmentServiceSdk from 'customer-segment-service-sdk';
import CustomerBrandServiceSdk from 'customer-brand-service-sdk';
import SessionManager from 'session-manager';

export default class Controller {

    _sessionManager:SessionManager;

    _$q;

    _$modalInstance;

    _accountServiceSdk:AccountServiceSdk;

    _iso3166Sdk:Iso3166Sdk;

    _countryList;

    _countrySubdivisionList = [];

    _selectedLevel1CustomerSegmentId;

    _level1CustomerSegmentList;

    _selectedLevel2CustomerSegmentId;

    _level2CustomerSegmentList;

    _filteredLevel2CustomerSegmentList;

    _level3CustomerSegmentList;

    _filteredLevel3CustomerSegmentList;

    _selectedLevel1CustomerBrandId;

    _level1CustomerBrandList;

    _level2CustomerBrandList;

    _filteredLevel2CustomerBrandList;

    _addCommercialAccountReqData = {};

    constructor(sessionManager:SessionManager,
                $q,
                $modalInstance,
                accountServiceSdk:AccountServiceSdk,
                iso3166Sdk:Iso3166Sdk,
                customerSegmentServiceSdk:CustomerSegmentServiceSdk,
                customerBrandServiceSdk:CustomerBrandServiceSdk) {

        if (!sessionManager) {
            throw new TypeError('sessionManager required');
        }
        this._sessionManager = sessionManager;

        if (!$q) {
            throw new TypeError('$q required');
        }
        this._$q = $q;

        if (!$modalInstance) {
            throw new TypeError('$modalInstance required');
        }
        this._$modalInstance = $modalInstance;

        if (!accountServiceSdk) {
            throw new TypeError('accountServiceSdk required');
        }
        this._accountServiceSdk = accountServiceSdk;

        if (!iso3166Sdk) {
            throw  new TypeError('iso3166Sdk required');
        }
        this._iso3166Sdk = iso3166Sdk;
        $q(resolve =>
            resolve(iso3166Sdk.listCountries())
        ).then(countryList => {
            this._countryList = countryList;
        });

        if (!customerSegmentServiceSdk) {
            throw new TypeError('customerSegmentServiceSdk required');
        }
        $q(
            resolve =>
                sessionManager
                    .getAccessToken()
                    .then(accessToken => resolve(accessToken))
        )
            .then(
                accessToken =>
                    $q.all([
                        customerSegmentServiceSdk
                            .listLevel1CustomerSegments(accessToken)
                            .then(level1CustomerSegmentList => {

                                this._level1CustomerSegmentList = level1CustomerSegmentList;

                            }),
                        customerSegmentServiceSdk
                            .listLevel2CustomerSegments(accessToken)
                            .then(level2CustomerSegmentList => {

                                this._level2CustomerSegmentList = level2CustomerSegmentList;

                            }),
                        customerSegmentServiceSdk
                            .listLevel3CustomerSegments(accessToken)
                            .then(level3CustomerSegmentList => {

                                this._level3CustomerSegmentList = level3CustomerSegmentList;

                            })
                    ])
            );

        if (!customerBrandServiceSdk) {
            throw new TypeError('customerBrandServiceSdk required');
        }
        $q(
            resolve =>
                sessionManager
                    .getAccessToken()
                    .then(accessToken => resolve(accessToken))
        )
            .then(accessToken =>
                $q.all([
                    customerBrandServiceSdk
                        .listLevel1CustomerBrands(accessToken)
                        .then(level1CustomerBrandList => {

                            this._level1CustomerBrandList = level1CustomerBrandList;

                        }),
                    customerBrandServiceSdk
                        .listLevel2CustomerBrands(accessToken)
                        .then(level2CustomerBrandList => {

                            this._level2CustomerBrandList = level2CustomerBrandList;

                        })
                ])
            );

    }

    get addCommercialAccountReqData() {
        return this._addCommercialAccountReqData;
    }

    get countryList() {
        return this._countryList;
    }

    get countrySubdivisionList() {
        return this._countrySubdivisionList;
    }

    get selectedLevel1CustomerBrandId() {
        return this._selectedLevel1CustomerBrandId;
    }

    set selectedLevel1CustomerBrandId(value) {
        this._selectedLevel1CustomerBrandId = value;
    }

    get level1CustomerBrandList() {
        return this._level1CustomerBrandList;
    }

    get filteredLevel2CustomerBrandList() {
        return this._filteredLevel2CustomerBrandList;
    }

    get selectedLevel1CustomerSegmentId() {
        return this._selectedLevel1CustomerSegmentId;
    }

    set selectedLevel1CustomerSegmentId(value) {
        this._selectedLevel1CustomerSegmentId = value;
    }

    get level1CustomerSegmentList() {
        return this._level1CustomerSegmentList;
    }

    get selectedLevel2CustomerSegmentId() {
        return this._selectedLevel2CustomerSegmentId;
    }

    set selectedLevel2CustomerSegmentId(value) {
        this._selectedLevel2CustomerSegmentId = value;
    }

    get filteredLevel2CustomerSegmentList() {
        return this._filteredLevel2CustomerSegmentList;
    }

    get filteredLevel3CustomerSegmentList() {
        return this._filteredLevel3CustomerSegmentList;
    }

    handleAddressCountryCodeChange() {

        // clear selected region
        this._addCommercialAccountReqData.address.regionIso31662Code = null;

        if (this._addCommercialAccountReqData.address.countryIso31661Alpha2Code) {

            this._$q(resolve =>
                resolve(
                    this._iso3166Sdk.listSubdivisionsWithAlpha2CountryCode(
                        this._addCommercialAccountReqData.address.countryIso31661Alpha2Code
                    )
                )
            ).then(countrySubdivisionList => {
                this._countrySubdivisionList = countrySubdivisionList;
            });

        }

    }

    handleCustomerBrandIdChange() {

        // clear selected level1/2/3 segments & request customerBrandId
        this._addCommercialAccountReqData.customerBrandId = null;
        this._addCommercialAccountReqData.customerSegmentId = null;
        this._selectedLevel1CustomerSegmentId = null;
        this._selectedLevel2CustomerSegmentId = null;

        // filter selectable sub brands
        this._filteredLevel2CustomerBrandList =
            this._level2CustomerBrandList
                .filter(level2CustomerBrand =>
                level2CustomerBrand.level1CustomerBrand.id == this._selectedLevel1CustomerBrandId);

        // if no level 2 brands under this level 1 brand then selection is complete;
        // set request customerBrandId.
        if (this._filteredLevel2CustomerBrandList.length < 1) {

            this._addCommercialAccountReqData.customerBrandId =
                this._selectedLevel1CustomerBrandId;

        }
        // if only 1 selectable sub brand then select it
        else if (this._filteredLevel2CustomerBrandList.length == 1) {

            this._addCommercialAccountReqData.customerBrandId =
                this._filteredLevel2CustomerBrandList[0].id;

        }
    }

    handleLevel1CustomerSegmentIdChange() {

        // clear selected level2/3 segments & request customerSegmentId
        this._addCommercialAccountReqData.customerBrandId = null;
        this._selectedLevel1CustomerBrandId = null;
        this._addCommercialAccountReqData.customerSegmentId = null;
        this._selectedLevel2CustomerSegmentId = null;

        // filter selectable level 2 customer segments
        this._filteredLevel2CustomerSegmentList =
            this._level2CustomerSegmentList
                .filter(level2CustomerSegment =>
                level2CustomerSegment.level1CustomerSegment.id == this._selectedLevel1CustomerSegmentId);

        // if no level 2 customer segments under this level 1 customer segment then selection is complete;
        // set request customerSegmentId.
        if (this._filteredLevel2CustomerSegmentList.length < 1) {
            this._addCommercialAccountReqData.customerSegmentId = this._selectedLevel1CustomerSegmentId;
        }
        // if only 1 selectable level 2 segment then select it
        else if (this._filteredLevel2CustomerSegmentList.length == 1) {

            this._selectedLevel2CustomerSegmentId =
                this._filteredLevel2CustomerSegmentList[0].id;

            this.handleLevel2CustomerSegmentIdChange();

        }

    }

    handleLevel2CustomerSegmentIdChange() {

        // clear selected level3 segments & request customerSegmentId
        this._addCommercialAccountReqData.customerSegmentId = null;

        // filter selectable level 3 customer segments
        this._filteredLevel3CustomerSegmentList =
            this._level3CustomerSegmentList
                .filter(level3CustomerSegment =>
                level3CustomerSegment.level2CustomerSegment.id == this._selectedLevel2CustomerSegmentId);

        // if no level 3 customer segments under this level 2 customer segment then selection is complete;
        // set request customerSegmentId.
        if (this._filteredLevel3CustomerSegmentList.length < 1) {

            this._addCommercialAccountReqData.customerSegmentId =
                this._selectedLevel2CustomerSegmentId;

        }
        // if only 1 selectable level 3 segment then select it
        else if (this._filteredLevel3CustomerSegmentList.length == 1) {

            this._addCommercialAccountReqData.customerSegmentId =
                this._filteredLevel3CustomerSegmentList[0].id;

        }

    }

    cancel() {
        this._$modalInstance.dismiss();
    }

    saveAndClose(form) {
        if (form.$valid) {

            this._sessionManager
                .getAccessToken()
                .then(
                    accessToken =>
                        this._accountServiceSdk
                            .addCommercialAccount(
                                AddCommercialAccountReqFactory
                                    .construct(this._addCommercialAccountReqData),
                                accessToken
                            )
                )
                .then(accountId =>
                    this._$modalInstance.close(accountId)
                );

        }
        else {
            form.$setSubmitted();
        }
    }
}

Controller.$inject = [
    'sessionManager',
    '$q',
    '$modalInstance',
    'accountServiceSdk',
    'iso3166Sdk',
    'customerSegmentServiceSdk',
    'customerBrandServiceSdk'
];