import {AddCommercialAccountReq} from 'account-service-sdk';
import {PostalAddressFactory} from 'postal-object-model';

export default class AddCommercialAccountReqFactory {

    static construct(data):AddCommercialAccountReq {

        const name = data.name;

        const address = PostalAddressFactory.construct(data.address);

        const phoneNumber = data.phoneNumber;

        const customerSegmentId = data.customerSegmentId;

        const customerBrandId = data.customerBrandId;

        return new AddCommercialAccountReq(
            name,
            address,
            phoneNumber,
            customerSegmentId,
            customerBrandId
        );

    }

}