import angular from 'angular';
import AddCommercialAccountModal from './addCommercialAccountModal';
import errorMessagesTemplate from './error-messages.html!text';
import 'angular-bootstrap';
import 'angular-messages';
import 'bootstrap/css/bootstrap.css!css'
import 'bootstrap'

angular
    .module(
        'addCommercialAccountModal.module',
        [
            'ui.bootstrap',
            'ngMessages'
        ]
    )
    .service(
        'addCommercialAccountModal',
        [
            '$modal',
            AddCommercialAccountModal
        ]
    )
    .run(
        [
            '$templateCache',
            $templateCache => {
                $templateCache.put(
                    'add-commercial-account-modal/error-messages.html',
                    errorMessagesTemplate
                );
            }
        ]
    );